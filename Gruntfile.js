module.exports = function (grunt) {

    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        cssmin:{
            options:{
                mediaMerging:true,
                sourceMap:true,
                aggressiveMerging:true,
            },
            //
            // Run during any 'deploy' command
            //
            development:{
                'options':{
                    mediaMerging:true,
                },
                files: [{
                    expand: true,
                    cwd: 'dist',
                    src: ['**/*.css', '!**/*.min.css'],
                    dest: 'dist',
                    ext: '.min.css'
                }]
            }
        },
        sass: {
            //
            // Run during development
            //
            development: {
                options: {
                    style: 'expanded',
                    noCache:false,
                    update:true,
                    loadPath:[
                        'bower_components/'
                    ]
                },
                files:[
                    {
                        expand: true,
                        cwd: 'src/',
                        src: ['grid.scss'],
                        dest: 'dist',
                        ext: '.css'
                    }
                ]
            },
        },
        watch: {
            //
            // Listens for changes to SCSS files
            //
            sass: {
                files: [
                    'src/**/*.scss',
                ],
                tasks: ['sass:development', 'cssmin:development'],
                options: {
                    interrupt: true,
                },
            }
        },

    });

    // 2. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // 3. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['sass:development', 'cssmin:development']);
    grunt.registerTask('development', ['sass:development', 'cssmin:development', 'watch:sass']);


};